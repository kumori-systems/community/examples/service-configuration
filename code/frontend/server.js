/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

const fs = require('fs')
const express = require('express')

const hostname = '0.0.0.0'
const port = process.env.HTTP_SERVER_PORT_ENV
const filesDirectory = "/kumori/files"

readFile = (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filesDirectory + "/" + filename, "utf8", (err, content) => {
      if (err) {
        reject(err)
        return
      }
      resolve(content)
    })
  })
}

createExpressApp = () => {
  var app = express()

  app.get('/file/:filename', (req, res) => {
    readFile(req.params.filename)
    .then((content) => {
      res.send(content)
    })
    .catch((err) => {
      res.status(503).send(`Unexpected error: ${err.message}`)
    })
  })

  app.get('/env/:varname', (req, res) => {
    try {
      value = process.env[req.params.varname]
      res.send(value)
    } catch (err) {
      res.status(503).send(`Unexpected error: ${err.message}`)
    }
  })

  app.get('/health', (req, res) => {
    console.log('Health request')
    res.send('OK')
  })

  app.use(function(req, res, next) {
    res.status(404).send('Not found')
  })

  return app
}

try {
  app = createExpressApp()
  app.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
  })
} catch (err) {
  console.log(`Error initializating component: ${err.message}`)
}
