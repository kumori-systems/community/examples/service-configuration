#!/bin/bash

REGISTRY="registry.gitlab.com"
IMG="${REGISTRY}/kumori-systems/community/image-registry/examples/service-configuration-frontend:v0.0.667"
#IMG="${REGISTRY}/kumori/kv3/examples/service-configuration/frontend:v0.0.666"
PORT="8080"
CONTPORT="8080"
KUMORIINSTANCEID="inst1"
KUMORIDEPLOYMENT="dep1"

case $1 in

'docker-build')
  docker build . -t ${IMG}
  ;;

'docker-rmi')
  docker rmi -f ${IMG}
  ;;

'docker-run')
	docker run --detach \
	--publish ${PORT}:${CONTPORT} \
	--env KUMORI_INSTANCE_ID=${KUMORIINSTANCEID} \
	--env KUMORI_DEPLOYMENT=${KUMORIDEPLOYMENT} \
	--env HTTP_SERVER_PORT_ENV=${PORT} \
	--name frontend \
	${IMG}
  ;;

'docker-push')
	docker login ${REGISTRY}
	docker push ${IMG}
	docker logout
  ;;

esac