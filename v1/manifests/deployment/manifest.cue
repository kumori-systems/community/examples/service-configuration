package deployment

import s ".../service:service"

#Deployment: {
  name: "serviceconfigdep"
  artifact: s.#Artifact
  config: {
    parameter: {
      // The first parameter is just a string (in this case, a long string)
      rawParameter: ###"""
        This is an example of mapped file using any format
        Language value: en.
        """###
      // The second parameter is a two-level dictionary, where values can
      // be strings or numbers
      dictionaryParameter: {
        keyA: {
          subkeyA1: "value-A"
        }
        keyB: {
          subkeyB1: 11
          subkeyB2: 22
          language: "en"
        }
      }
    }
    resource: {}
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
