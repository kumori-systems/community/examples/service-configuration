package service

import (
  f ".../frontend:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    config: {
      parameter: {
        // The first parameter is just a string
        rawParameter: string
        // The second parameter is a two-level dictionary, where values can
        // be strings or numbers
        dictionaryParameter: [string]: [string]: string | number
      }
      resource: {}
    }

    role: frontend: {
      artifact: f.#Artifact
      config: {
        // Spread of service parameters into role parameters
        parameter: {
          rawParameter: description.config.parameter.rawParameter
          dictionaryParameter: description.config.parameter.dictionaryParameter
        }
        resource: {}
      }
    }

    srv: server: restapi: { protocol: "http", port: 80 }
    connect: {
      inbound: {
        as: "lb"
  			from: self: "restapi"
        to: frontend: "restapi": _
      }
    }
  }
}
