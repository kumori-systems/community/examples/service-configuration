package component

import (
  "kumori.systems/utils"
)

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
    }

    config: parameter: {
      // The first parameter is just a string
      rawParameter: string
      // The second parameter is a two-level dictionary, where values can
      // be strings or numbers
      dictionaryParameter: [string]: [string]: string | number
    }

    size: bandwidth: { size: 10, unit: "M" }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 60000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: {
      frontend: {
        name: "frontend"
        image: {
          hub: { name: "", secret: "" }
          tag: "kumoripublic/examples-service-configuration-frontend:v1.0.4"
        }
        mapping: {
          // 	Mapping of configuration parameters to files inside the container
          filesystem: {

            // Example 1: Map the rawParameter into a non-format file
            "/kumori/files/example1.txt": {
              data: value: description.config.parameter.rawParameter
              format: "text"
            }

            // Example 2: Map the dictionaryParameter into a JSON file.
            // JSON is a platform supported format
            "/kumori/files/example2.json": {
              data: value: description.config.parameter.dictionaryParameter
              format: "json"
            }

            // Example 3: Map the dictionaryParameter into a YAML file.
            // YAML is a platform supported format
            "/kumori/files/example3.yaml": {
              data: value: description.config.parameter.dictionaryParameter
              format: "yaml"
            }

            // Example 4: Map the dictionaryParameter into a "key=value" file.
            // This format is not supported by the platform, so we use the
            // "FlattenJson" conversion available in the utils module.
            // (see https://gitlab.com/kumori-systems/community/libraries/cue-utils/-/tree/v1.1.1)
            let _flattenContent = {
              utils.#FlattenJson &
              {#p: description.config.parameter.dictionaryParameter}
            }.#value
            "/kumori/files/example4.properties": {
              data: value: _flattenContent
              format: "text"
            }
          }
          env: {
            // Example 5: Map the rawParameter into a environment variable
            RAW_PARAMETER: value: description.config.parameter.rawParameter

            // This component requires an environment variable containing the
            // the port to listen requests. Because "port" is a numeric value,
            // we convert it to string using CUE facilities (string interpolation)
            HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
          }
        }
        size: {
          memory: { size: 100, unit: "M" }
          mincpu: 100
          cpu: { size: 200, unit: "m" }
        }
      }
    }
  }
}
