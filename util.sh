#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="serviceconfinb"
DEPLOYNAME="serviceconfdep"
DOMAIN="serviceconfdomain"
SERVICEURL="serviceconf-${CLUSTERNAME}.${REFERENCEDOMAIN}"

# This repository contains two examples
#MANIFESTS="./v1/manifests"
MANIFESTS="./v2/manifests"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_UTILS_VERSION="1.1.1"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd $MANIFESTS
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency --delete kumori.systems/utils
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  ${KAM_CMD} mod dependency kumori.systems/utils/@${KUMORI_UTILS_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to generate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t $MANIFESTS
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t $MANIFESTS $DEPLOYNAME -- \
    --comment "Service conf example" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'update-service')
  ${KAM_CMD} service update -d deployment -t $MANIFESTS $DEPLOYNAME -- \
    --comment "Updating service conf example" \
    --wait 5m
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME
  ;;

# Test the mappings service
'test')
  echo "-------------------------------------------------"
  curl https://${SERVICEURL}/file/example1.txt; echo
  echo "-------------------------------------------------"
  curl https://${SERVICEURL}/file/example2.json; echo
  echo "-------------------------------------------------"
  curl https://${SERVICEURL}/file/example3.yaml; echo
  echo "-------------------------------------------------"
  curl https://${SERVICEURL}/file/example4.properties; echo
  echo "-------------------------------------------------"
  curl https://${SERVICEURL}/env/RAW_PARAMETER; echo
  echo "-------------------------------------------------"
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m --force
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'undeploy-all')
  $0 unlink
  $0 undeploy-inbound
  $0 undeploy-service
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
