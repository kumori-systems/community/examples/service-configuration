package service

import (
  f ".../frontend:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    config: {
      // In this case, only the "language" parameter is defined in the service
      parameter: language: string
      resource: {}
    }

    role: frontend: {
      artifact: f.#Artifact
      // Spread of service parameters into role parameters
      config: {
        parameter: language: description.config.parameter.language
        resource: {}
      }
    }

    srv: server: restapi: { protocol: "http", port: 80 }
    connect: {
      inbound: {
        as: "lb"
  			from: self: "restapi"
        to: frontend: "restapi": _
      }
    }
  }
}
