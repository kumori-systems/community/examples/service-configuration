package deployment

import s ".../service:service"

#Deployment: {
  name: "serviceconfigdep"
  artifact: s.#Artifact
  config: {
    // In this case, only the "language" parameter must be provided within the deployment
    parameter: language: "en"
    resource: {}
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
