package kmodule

{
	local:  true
	domain: "kumori.systems"
	module: "serviceconfiguration"
	cue:    "0.4.2"
	version: [
		1,
		0,
		6,
	]
	dependencies: {
		"kumori.systems/kumori": {
			target: "kumori.systems/kumori/@1.1.6"
			query:  "1.1.6"
		}
		"kumori.systems/utils": {
			target: "kumori.systems/utils/@1.1.1"
			query:  "1.1.1"
		}
	}
	sums: {
		"kumori.systems/kumori/@1.1.6": "jsXEYdYtlen2UgwDYbUCGWULqQIigC6HmkexXkyp/Mo="
		"kumori.systems/utils/@1.1.1":  "C+UziBMKm+ylsJuHxunfBbOFavAQh+Vhs7lfcEzQciA="
	}
	spec: [
		1,
		0,
	]
}
