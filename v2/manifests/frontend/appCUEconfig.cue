package component

// This is an example of content that must be mapped in the instance.
// The format of the content is CUE, and will be mapped in the instance with
// an specific format (see manifest.cue).
// The role configuration parameter "language" is used in the content.

#appCUEConfigTemplate: {
  #params: {
    language: string
  }

  keyA: {
    subkeyA1: "value-A-one"
  }
  keyB: {
    subkeyB1: "value-B-one"
    subkeyB2: "value-B-two"
    language: #params.language
  }
}