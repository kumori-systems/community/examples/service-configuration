package component

// This is an example of content that must be mapped in the instance.
// The format of the content doesn't matter (it is just text), and will be
// mapped in the instance using its original format.
// The role configuration parameter "language" is included in the text, using
// "CUE string interpolation".

#appRawConfigTemplate: {
  #params: {
    language: string
  }

  value: ###"""
  This is an example of mapped file using any format
  Language value: \###(#params.language)
  """###
}