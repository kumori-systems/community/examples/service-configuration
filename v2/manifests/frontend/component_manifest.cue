package component

import (
  "kumori.systems/utils"
)

#Artifact: {
  ref: name: "frontend"

  description: {

    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
    }

    config: parameter: {
      // In this case, only the "language" parameter is defined in the service
      language: string
    }

    // Derived parameters using templates.
    //
    // Configuration is defined in two template files, the processing of
    // which we store in two temporary variables:
    //
    // - _appRawConfig is a free-format content, created using a template
    //   filled with the "language" parameter
    let _appRawConfig = ({
      #appRawConfigTemplate & { #params: language: description.config.parameter.language }
    }).value
    //
    // - _appCUEConfig is a cue-format content, created using a template
    //   filled with the "language" parameter. This config will be maped
    //   in several config formats (see below)
    let _appCUEConfig = ({
      #appCUEConfigTemplate & { #params: language: description.config.parameter.language }
    })
    //
    // - _flattenContent is a "key=value" content, created using a template
    //   filled with the "language" parameter.
    //   This format is not supported by the platform, so we use the
    //   "FlattenJson" conversion available in the utils module.
    //   (see https://gitlab.com/kumori-systems/community/libraries/cue-utils/-/tree/v1.1.1)
    let _flattenContent = { utils.#FlattenJson & {#p: _appCUEConfig} }.#value

    size: bandwidth: { size: 10, unit: "M" }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 60000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: {
      frontend: {
        name: "frontend"
        image: {
          hub: { name: "", secret: "" }
          tag: "kumoripublic/examples-service-configuration-frontend:v1.0.4"
        }
        mapping: {

          // 	Mapping of configuration parameters to files inside the container
          filesystem: {

            // Example 1: Map the appRawConfig config parameter into a non-format
            // file
            "/kumori/files/example1.txt": {
              data: value: _appRawConfig
              format: "text"
            }

            // Example 2: Map the appCUEConfig config parameter into a JSON file
            // JSON is a platform supported format
            "/kumori/files/example2.json": {
              data: value: _appCUEConfig
              format: "json"
            }

            // Example 3: Map the appCUEConfig config parameter into a YAML file
            // YAML is a platform supported format
            "/kumori/files/example3.yaml": {
              data: value: _appCUEConfig
              format: "yaml"
            }

            // Example 4: Map the appCUEConfig config parameter into a "key=value"
            // file.
            // This isn't a platform supported format, so has been generated
            // using external utilities.
            "/kumori/files/example4.properties": {
              data: value: _flattenContent
              format: "text"
            }
          }
          env: {
            // Example 5: Map the appRawConfig into a environment variable
            RAW_PARAMETER: value: _appRawConfig

            // This component requires an environment variable containing the
            // the port to listen requests. Because "port" is a numeric value,
            // we convert it to string using CUE facilities (string interpolation)
            HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
          }
        }
        size: {
          memory: { size: 100, unit: "M" }
          mincpu: 100
          cpu: { size: 200, unit: "m" }
        }
      }
    }
  }
}
